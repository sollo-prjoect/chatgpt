import openai

openai.api_key = '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'



def get_respons_text(request, result = None, max_tokens=4000, model_engine="text-davinci-003"):
    '''request - message, max_tokens - информационный вес токена, model_engine - модель поведения'''
    completion = 0
    try:
        # создаём и кадаем запрос
        completion = openai.Completion.create(
            engine=model_engine,
            prompt=request,
            max_tokens=max_tokens
        )
        print(len(completion.choices))
        response = completion.choices[0].text
    except Exception as e:
        print(e)
        if 'token' in e.args[0]:
            if result != None:
                return result.append("Не удалось создать запрос. Попробуёте сократить текст")
            return "Не удалось создать запрос. Попробуёте сократить текст"
        response = get_respons_text(request, max_tokens=max_tokens, model_engine=model_engine)
    # получаем ответ
    if result != None:
        result.append(response)
        return result
    return response